//
//  SYAlertView.m
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "SYAlertView.h"
#import "UIImage+SY.h"
#import <objc/runtime.h>
#import <QuartzCore/QuartzCore.h>

const int kSYAlertViewButtonSize = 25;
const int kSYAlertViewCornerRadius = 10;

@interface SYAlertView()
{
    UIButton *_closeButton;
    UIView *_containerView;
    CGRect _contentViewFrame;
}
@end

@implementation SYAlertView

- (id) init
{
    self = [super init];
    if(self)
    {
        _containerView = [[UIView alloc] init];
        _containerView.clipsToBounds = YES;
        _containerView.backgroundColor = [UIColor whiteColor];
        _containerView.layer.cornerRadius = kSYAlertViewCornerRadius;
        [self addSubview:_containerView];
        
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setImage:[UIImage closeButtonImage] forState:UIControlStateNormal];
        _closeButton.frame = CGRectMake(0, 0, kSYAlertViewButtonSize, kSYAlertViewButtonSize);
        [_closeButton addTarget:self action:@selector(SY_closeButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_closeButton];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setNeedsLayout) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    return self;
}

- (void) SY_closeButtonAction
{
    [self dismissWithClickedButtonIndex:0 animated:YES];
}

- (id) initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    NSAssert(NO, @"SYAlertView does not support this init method.");
    return [self init];
}

-(void)layoutSubviews
{
    CGPoint center = self.center;
    self.frame = self.superview.bounds;
    self.center = center;
    
    float contentViewOffset = (kSYAlertViewButtonSize + kSYAlertViewCornerRadius) / 2;

    CGPoint topLeft = [self convertPoint:CGPointZero fromView:self.superview];
    CGPoint bottomRight = [self convertPoint:CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame)) fromView:self.superview];
    CGSize size = CGSizeMake(bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);
    _containerView.frame =  CGRectMake(0, 0, MIN(_contentViewFrame.size.width, size.width - contentViewOffset * 2), MIN(_contentViewFrame.size.height, size.height - contentViewOffset * 2));
    center = [self convertPoint:self.center fromView:self.superview];
    _containerView.center = center;
    _contentView.frame = _containerView.bounds;
    _closeButton.center = CGPointMake(_containerView.frame.origin.x, _containerView.frame.origin.y);
    
    for(UIView *view in self.subviews)
    {
        if ([view isKindOfClass:[UIImageView class]])
        {
            view.hidden = YES;
        }
    }
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setContentView:(UIView *)contentView
{
    [_contentView removeFromSuperview];
    _contentView = contentView;
    _contentViewFrame = _contentView.frame;
    [_containerView addSubview:_contentView];
}

@end
