//
//  SYCoreDataManager.h
//  LPScedule
//
//  Created by Sviatoslav Yakymiv on 3/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SYDefines.h"

@interface SYCoreDataManager : NSObject
{
    @protected
    NSString *_modelName;
    NSString *_storageName;
    NSSearchPathDirectory _searchPathDirectory;
}

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

SINGLETON_INTERFACE(sharedManager);

- (id) createInstanceOfClass:(Class) instanceClass;
- (id) instancesOfClass: (Class) instancesClass;
- (id) instancesOfClass:(Class)instancesClass filteredUsingPredicate:(NSPredicate*) predicate;

- (void) removeAllInstancesOfClass: (Class) instancesClass;
- (void) removeObject: (id) object;
- (void) removeObjects: (NSArray*) instances;

- (void) handlePersistentStoreError:(NSError*) error;

- (void) save;
- (void) reset;

- (NSFetchedResultsController*) createFetchedResultsControllerForClass:(Class)instanceClass sectionNameKeyPath: (NSString*) sectionNameKeyPath filteredUsingPredicate:(NSPredicate *)predicate sortedUsingDescriptors:(NSArray *)sortDescriptors;
- (NSFetchedResultsController*) createFetchedResultsControllerForClass:(Class)instanceClass filteredUsingPredicate:(NSPredicate *)predicate sortedUsingDescriptors:(NSArray *)sortDescriptors;
- (NSFetchedResultsController*) createFetchedResultsControllerForClass:(Class)instanceClass sortedUsingDescriptors:(NSArray *)sortDescriptors;

@end
