//
//  UITableViewController+SY.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/11/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewController (SY)<NSFetchedResultsControllerDelegate>

@end
