//
//  NSDictionary+SY.m
//  LPScedule
//
//  Created by Sviatoslav Yakymiv on 3/9/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "NSDictionary+SY.h"
#import <objc/runtime.h>

@implementation NSDictionary (SY)

//+ (void)load {
//    Method initWithObjectsForKeysCount = class_getInstanceMethod(self, @selector(initWithObjects:forKeys:count:));
//    Method logAddObject = class_getInstanceMethod(self, @selector(logAddObject:));
//    method_exchangeImplementations(addObject, logAddObject);
//}

// Determine if we can handle the unknown selector sel
- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel
{
    id	stringSelector = NSStringFromSelector(sel);
    int	parameterCount = [[stringSelector componentsSeparatedByString:@":"] count]-1;
    // Zero argument, forward to valueForKey:
    if (parameterCount == 0)
        return [super methodSignatureForSelector:@selector(valueForKey:)];
    // One argument starting with set, forward to setValue:forKey:
    if (parameterCount == 1 && [stringSelector hasPrefix:@"set"])
        return [super methodSignatureForSelector:@selector(setValue:forKey:)];
    // Discard the call
    return nil;
}
// Call valueForKey: and setValue:forKey:
- (void)forwardInvocation:(NSInvocation *)invocation
{
    id	stringSelector = NSStringFromSelector([invocation selector]);
    int	parameterCount = [[stringSelector componentsSeparatedByString:@":"] count]-1;
    // Forwarding to valueForKey:
    if (parameterCount == 0)
    {
        id value = [self valueForKey:NSStringFromSelector([invocation selector])];
        [invocation setReturnValue:&value];
    }
    // Forwarding to setValue:forKey:
    if (parameterCount == 1)
    {
        id value;
        // The first parameter to an ObjC method is the third argument
        // ObjC methods are C functions taking instance and selector as their first two arguments
        [invocation getArgument:&value atIndex:2];
        // Get key name by converting setMyValue: to myValue
        id key = [NSString stringWithFormat:@"%@%@",  [[stringSelector substringWithRange:NSMakeRange(3, 1)] lowercaseString], [stringSelector substringWithRange:NSMakeRange(4, [stringSelector length]-5)]];
        // Set
        [self setValue:value forKey:key];
    }
}

@end