//
//  ViewController.h
//  TabBarTest
//
//  Created by Sviatoslav Yakymiv on 3/16/13.
//  Copyright (c) 2013 Lemberg Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SYToolbarViewController : UIViewController

@property (nonatomic, strong) NSArray *viewControllers;
@property (nonatomic, weak) UIViewController *selectedViewController;
@property (nonatomic) NSUInteger selectedIndex;

@end
