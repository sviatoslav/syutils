//
//  SYAlertView.h
//  LPSchedule
//
//  Created by Sviatoslav Yakymiv on 5/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SYAlertView : UIAlertView

@property (nonatomic, strong) UIView *contentView;

@end