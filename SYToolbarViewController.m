//
//  ViewController.m
//  TabBarTest
//
//  Created by Sviatoslav Yakymiv on 3/16/13.
//  Copyright (c) 2013 Lemberg Solutions. All rights reserved.
//

#import "SYToolbarViewController.h"
#import <QuartzCore/QuartzCore.h>

int const kSYTabBarHeight = 44;


@interface SYToolbarViewController ()

@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIView *contentView;


@end

@implementation SYToolbarViewController

- (UIToolbar *)toolbar
{
    if(_toolbar == nil)
    {
        _toolbar = [[UIToolbar alloc] init];
        _toolbar.barStyle = UIBarStyleBlack;
    }
    return _toolbar;
}

- (UIView *)contentView
{
    if(_contentView == nil) _contentView = [[UIView alloc] init];
    return _contentView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.toolbar];
    [self.view addSubview:self.contentView];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.selectedIndex = self.selectedIndex;
    self.contentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - kSYTabBarHeight);
    self.toolbar.frame = CGRectMake(0, self.view.frame.size.height - kSYTabBarHeight, self.view.frame.size.width, kSYTabBarHeight);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.selectedIndex = self.selectedIndex;
    self.contentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - kSYTabBarHeight);
    self.toolbar.frame = CGRectMake(0, self.view.frame.size.height - kSYTabBarHeight, self.view.frame.size.width, kSYTabBarHeight);
}

- (void)setViewControllers:(NSArray *)viewControllers
{
    NSMutableArray *items = [NSMutableArray array];
    UIBarButtonItem *firstItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [items addObject:firstItem];
    
    int currentButtonTag = 1;
    
    for(UIViewController *viewController in viewControllers)
    {
        UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:viewController.title style:UIBarButtonItemStylePlain target:self action:@selector(SY_toolbarButtonItemAction:)];
        button.tag = currentButtonTag ++;
        [items addObject:button];
        
        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [items addObject:space];
    }
    [self.toolbar setItems:items animated:NO];
    
    _viewControllers = viewControllers;
    self.selectedIndex = 0;
}

- (void)setSelectedViewController:(UIViewController *)selectedViewController
{
    self.selectedIndex = [self.viewControllers indexOfObject:selectedViewController];
}

- (void) setSelectedIndex:(NSUInteger)selectedIndex
{
    _selectedIndex = selectedIndex < self.viewControllers.count ? selectedIndex : 0;
    if(_selectedIndex >= self.viewControllers.count) return;
    UIViewController *toViewController = self.viewControllers[self.selectedIndex];
    [self SY_transitionFromViewController:self.selectedViewController toViewController:toViewController];
    _selectedViewController = toViewController;
    for(UIBarButtonItem *item in self.toolbar.items)
    {
        item.style = item.tag == self.selectedIndex + 1 ? UIBarButtonItemStyleDone : UIBarButtonItemStyleBordered;
    }
    for (UIView *view in self.toolbar.subviews) {
        if([view isKindOfClass:NSClassFromString(@"UIToolbarTextButton")])
        {
            view.transform = CGAffineTransformMakeRotation(-[self SY_roatationAngleForInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]]);
        }
    }
}

- (void) SY_toolbarButtonItemAction: (UIBarButtonItem*) sender
{
    self.selectedIndex = sender.tag - 1;
}

- (void) SY_transitionFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    if(toViewController == nil || toViewController.parentViewController == self)
    {
        return;
    }
    
    toViewController.view.frame = self.contentView.bounds;
    toViewController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    if(fromViewController.parentViewController == self && fromViewController != toViewController)
    {
        [fromViewController willMoveToParentViewController:nil];
        [self addChildViewController:toViewController];
        [self transitionFromViewController:fromViewController toViewController:toViewController duration:0 options:UIViewAnimationOptionCurveLinear animations:nil completion:^(BOOL finished) {
            [toViewController didMoveToParentViewController:self];
            [fromViewController removeFromParentViewController];
        }];
    }
    else
    {
        [self addChildViewController:toViewController];
        [self.contentView addSubview:toViewController.view];
        [toViewController didMoveToParentViewController:self];
    }
    
}

- (CGRect) SY_contentViewFrameForInterfaceOrientation: (UIInterfaceOrientation) orientation
{
    if(UIInterfaceOrientationIsLandscape(orientation))
    {
        return CGRectMake( orientation == UIInterfaceOrientationLandscapeLeft ? kSYTabBarHeight : 0, 0, self.view.frame.size.width - kSYTabBarHeight, self.view.frame.size.height);
    }
    else
    {
        return CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - kSYTabBarHeight);
    }
}

- (float) SY_roatationAngleForInterfaceOrientation: (UIInterfaceOrientation) orientation
{
    float alpha = 0.0;
    switch (orientation) {
        case UIInterfaceOrientationLandscapeLeft:
            alpha = M_PI_2;
            break;
        case UIInterfaceOrientationLandscapeRight:
            alpha = -M_PI_2;
            break;
        default:
            alpha = 0.0;
            break;
    }
    return alpha;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    self.toolbar.transform = CGAffineTransformMakeRotation(0);
    [self.view.window addSubview:self.toolbar];
    CGPoint origin = [self.view convertPoint:self.toolbar.frame.origin toView:self.view.window];
    CGRect frame;
    frame.origin = origin;
    frame.size = self.toolbar.frame.size;
    self.toolbar.frame = frame;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //bottom view frame calculation
    CGPoint topRightCorner = [self.view convertPoint:CGPointZero toView:self.view.window];
    CGPoint bottomLeftCorner = [self.view convertPoint:CGPointMake(self.view.frame.size.width, self.view.frame.size.height) toView:self.view.window];
    int xOffset = MIN(topRightCorner.x, bottomLeftCorner.x);
    int width = MAX(topRightCorner.x, bottomLeftCorner.x) - MIN(topRightCorner.x, bottomLeftCorner.x);
    CGRect bottomViewFrame = CGRectMake( xOffset, self.toolbar.frame.origin.y, width, self.toolbar.frame.size.height);
    [UIView animateWithDuration:duration animations:^{
        self.toolbar.frame = bottomViewFrame;
        _contentView.frame = [self SY_contentViewFrameForInterfaceOrientation:toInterfaceOrientation];
        for(UIView *view in self.toolbar.subviews)
        {
            if([view isKindOfClass:NSClassFromString(@"UIToolbarTextButton")])
            {
                view.transform = CGAffineTransformMakeRotation(-[self SY_roatationAngleForInterfaceOrientation:toInterfaceOrientation]);
            }
        }
    }];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.view addSubview:self.toolbar];
    CGPoint origin = [self.view.window convertPoint:self.toolbar.frame.origin toView:self.view];
    CGRect frame;
    frame.origin = origin;
    frame.size = self.toolbar.frame.size;
    self.toolbar.layer.anchorPoint = CGPointZero;
    self.toolbar.frame = frame;
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    self.toolbar.transform = CGAffineTransformMakeRotation([self SY_roatationAngleForInterfaceOrientation:orientation]);
}

@end
