//
//  NSNotificationCenter+SY.m
//  LPScedule
//
//  Created by Sviatoslav Yakymiv on 3/9/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "NSNotificationCenter+SY.h"

@implementation NSNotificationCenter (SY)

+ (void) addObserver:(id)object withSelector:(SEL)selector forName:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] addObserver:object selector:selector name:name object:nil];
}

+ (void) addObserverForName: (NSString*) name usingBlock:(void(^)(NSNotification* notification)) block
{
    [[NSNotificationCenter defaultCenter] addObserverForName:name object:nil queue:nil usingBlock:block];
}

@end
