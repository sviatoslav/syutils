//
//  SYDefines.h
//  LPScedule
//
//  Created by Sviatoslav Yakymiv on 3/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#ifndef SYDefines_h
#define SYDefines_h

#pragma mark - Singleton
#define SINGLETON_INTERFACE(NAME) + (id) NAME;
#define SINGLETON_IMPLEMENTATION(NAME) + (id) NAME { \
    static NSMutableDictionary *NAME##sDictionary;\
    static dispatch_once_t onceToken;\
    dispatch_once(&onceToken, ^{ NAME##sDictionary = [[NSMutableDictionary alloc] init]; });\
    id result = NAME##sDictionary[NSStringFromClass([self class])];\
    if(result == nil) {\
        @synchronized([self class]) {\
            if(result == nil) {\
                result = [[[self class] alloc] init];\
                NAME##sDictionary[NSStringFromClass([self class])] = result;\
            }\
        }\
    }\
    return result;\
}



#endif
