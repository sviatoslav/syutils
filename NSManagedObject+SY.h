//
//  NSManagedObject+SY.h
//  LPScedule
//
//  Created by Sviatoslav Yakymiv on 3/9/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (SY)

@property (nonatomic) NSDictionary *dictionaryValue;

@end
