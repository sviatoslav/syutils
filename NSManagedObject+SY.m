//
//  NSManagedObject+SY.m
//  LPScedule
//
//  Created by Sviatoslav Yakymiv on 3/9/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "NSManagedObject+SY.h"

@implementation NSManagedObject (SY)

@dynamic dictionaryValue;

- (NSDictionary*) dictionaryValue
{
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    NSArray *attributes = self.entity.attributesByName.allKeys;
    for(NSString *attribute in attributes)
    {
        id value = [self valueForKey:attribute];
        if(value)
        {
            [result setValue:value forKey:attribute];
        }
    }
    return [NSDictionary dictionaryWithDictionary:result];
}

- (void)setDictionaryValue:(NSDictionary *)dictionaryValue
{
    NSArray *attributes = self.entity.attributesByName.allKeys;
    for(NSString *attribute in attributes)
    {
        id value = [dictionaryValue valueForKey:attribute];
        if(value && ([value isKindOfClass:[NSNull class]] == NO))
        {
            [self setValue:value forKey:attribute];
        }
    }
}

@end
