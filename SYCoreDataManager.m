//
//  SYCoreDataManager.m
//  LPScedule
//
//  Created by Sviatoslav Yakymiv on 3/8/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import "SYCoreDataManager.h"
#import "NSNotificationCenter+SY.h"
#import "LPLesson.h"

#pragma mark - SYBackgroundContextStorage

@interface SYBackgroundContextStorage : NSObject

@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSThread *thread;

@end

@implementation SYBackgroundContextStorage

@end

#pragma mark -

@interface SYCoreDataManager()
{
    NSMutableArray *_backgroundContexts;
}

@end

@implementation SYCoreDataManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (id) init
{
    self = [super init];
    if(self)
    {
        _storageName = @"LPSchedule";
        _modelName = @"LPSchedule";
        _searchPathDirectory = NSDocumentDirectory;
        //we should call this method because we should create main managed object context
        [self performSelectorOnMainThread:@selector(managedObjectContext) withObject:nil waitUntilDone:YES];
//        [self performSelectorInBackground:@selector(managedObjectContext) withObject:nil];
    }
    return self;
}

- (void)save
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void) reset
{
    [self.managedObjectContext reset];
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if(![NSThread isMainThread])
    {
        return [self SY_backgroundManagedObjectContext];
    }
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

SINGLETON_IMPLEMENTATION(sharedManager);

- (NSManagedObjectContext*) SY_backgroundManagedObjectContext
{
    NSAssert(![NSThread isMainThread], @"This method should not be called from main thread.");
    if([NSThread isMainThread]) return nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"thread == %@", [NSThread currentThread]];
    NSArray *currentThreadManagedObjectContextStorages = [_backgroundContexts filteredArrayUsingPredicate:predicate];
    NSAssert([currentThreadManagedObjectContextStorages count] <= 1, @"Multiple managed object contexts per thread");
    SYBackgroundContextStorage *storage = currentThreadManagedObjectContextStorages.lastObject;
    if(storage == nil)
    {
        storage = [[SYBackgroundContextStorage alloc] init];
        storage.thread = NSThread.currentThread;
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (coordinator != nil) {
            NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            context.parentContext = _managedObjectContext;
            storage.context = context;
            if(_backgroundContexts == nil)
            {
                _backgroundContexts = [[NSMutableArray alloc] init];
                [NSNotificationCenter addObserverForName:NSThreadWillExitNotification usingBlock:^(NSNotification *notification) {
                    NSArray *finishedStorages = [_backgroundContexts filteredArrayUsingPredicate:predicate];
                    [_backgroundContexts removeObjectsInArray:finishedStorages];
                }];
            }
            [_backgroundContexts addObject:storage];
        }
        else return nil;
    }
    return storage.context;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    NSAssert(_modelName != nil, @"_modelName should not be nil");
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:_modelName withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    NSAssert(_storageName != nil, @"_storageName should not be nil");
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self SY_storageURL] URLByAppendingPathComponent:_storageName];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        [self handlePersistentStoreError:error];
    }
    
    return _persistentStoreCoordinator;
}

- (void) handlePersistentStoreError:(NSError*) error
{
    NSURL *storeURL = [[self SY_storageURL] URLByAppendingPathComponent:_storageName];
    [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
    [self persistentStoreCoordinator];
}

- (id) createInstanceOfClass:(Class)instanceClass
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass(instanceClass) inManagedObjectContext: managedObjectContext];
    NSManagedObject *managedObject = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext: managedObjectContext];
    return managedObject;
}

- (id) instancesOfClass:(Class)instancesClass
{
    return [self instancesOfClass:instancesClass filteredUsingPredicate:nil];
}

- (id) instancesOfClass:(Class)instancesClass filteredUsingPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(instancesClass)];
    [fetchRequest setPredicate:predicate];
    return [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
}

- (void)removeObject:(id)object
{
    [self.managedObjectContext deleteObject:object];
}

- (void) removeAllInstancesOfClass:(Class)instancesClass
{
    NSArray *instances = [self instancesOfClass:instancesClass];
    [self removeObjects:instances];
}

- (void) removeObjects:(NSArray *)instances
{
    for(id object in instances)
    {
        [self removeObject:object];
    }
}

- (NSFetchedResultsController*) createFetchedResultsControllerForClass:(Class)instanceClass sectionNameKeyPath: (NSString*) sectionNameKeyPath filteredUsingPredicate:(NSPredicate *)predicate sortedUsingDescriptors:(NSArray *)sortDescriptors
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(instanceClass)];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext sectionNameKeyPath:sectionNameKeyPath
                                                   cacheName:nil];
    [theFetchedResultsController performFetch:nil];
    return theFetchedResultsController;
}


- (NSFetchedResultsController*) createFetchedResultsControllerForClass:(Class)instanceClass filteredUsingPredicate:(NSPredicate *)predicate sortedUsingDescriptors:(NSArray *)sortDescriptors
{
    return [self createFetchedResultsControllerForClass:instanceClass sectionNameKeyPath:nil filteredUsingPredicate:predicate sortedUsingDescriptors:sortDescriptors];
}


- (NSFetchedResultsController*) createFetchedResultsControllerForClass:(Class)instanceClass sortedUsingDescriptors:(NSArray *)sortDescriptors
{
    return [self createFetchedResultsControllerForClass:instanceClass filteredUsingPredicate:nil sortedUsingDescriptors:sortDescriptors];
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)SY_storageURL
{
    return [[[NSFileManager defaultManager] URLsForDirectory:_searchPathDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
