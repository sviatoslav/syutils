//
//  NSNotificationCenter+SY.h
//  LPScedule
//
//  Created by Sviatoslav Yakymiv on 3/9/13.
//  Copyright (c) 2013 Sviatoslav Yakymiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNotificationCenter (SY)

+ (void) addObserver:(id) object withSelector:(SEL) selector forName:(NSString*) name;
+ (void) addObserverForName: (NSString*) name usingBlock:(void(^)(NSNotification* notification)) block;

@end
